import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class StringCalculatorTest {

    StringCalculator calc;

    @BeforeEach
    void initEach() {
        calc = new StringCalculator();
    }

    @Test
    void ShouldReturn0_whenStringEmpty() {
        assertCalculation(0, "");
    }

    @Test
    void ShouldReturnNumber_whenStringIsSingleNumber() {
        assertCalculation(0, "0");
        assertCalculation(1, "1");
        assertCalculation(10, "10");
        assertCalculation(32, "32");
        assertCalculation(1446, "1446");
    }

    @Test
    void ShouldReturnSum_whenStringIsTwoNumbers() {
        assertCalculation(0, "0 0");
        assertCalculation(1, "1 0");
        assertCalculation(1, "0 1");
        assertCalculation(3, "2 1");
        assertCalculation(3, "1 2");
    }

    @Test
    void ShouldThrowIllegalArgumentException_whenInputIsNull() {
        assertIllegal(null);
    }

    @Test
    void ShouldThrowIllegalArgumentException_whenInputContainsNotSpaceOrNumber() {
        assertIllegal(",");
        assertIllegal("2# ");
        assertIllegal("+ ");
        assertIllegal("1.3");
        assertIllegal("   a ");
    }

    @Test
    void ShouldThrowIllegalArgumentException_whenInputMoreThanTwoNumbers() {
        assertIllegal("0 0 0");
        assertIllegal("0 0 1");
        assertIllegal("1 0 1");
        assertIllegal("1 1 1 1");
        assertIllegal("1 2 3 4 5");
    }

    private void assertCalculation(int expected, String input) {
        Assertions.assertEquals(expected, calc.calculate(input));
    }

    private void assertIllegal(String input) {
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> calc.calculate(input)
        );
    }

}
