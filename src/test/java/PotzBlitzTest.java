import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

public class PotzBlitzTest {

    PotzBlitz potzBlitz;

    @BeforeEach
    void initEach() {
        potzBlitz = new PotzBlitz();
    }

    @Test
    public void testPotzBlitzWithNumber() {
        String result = potzBlitz.calculate(1);
        assertEquals("1", result);
    }

    @Test
    public void testPotzBlitzForMultiplesOfThree() {
        String result = potzBlitz.calculate(9);
        assertEquals("Potz", result);
    }

    @Test
    public void testPotzBlitzForMultiplesOfFive() {
        String result = potzBlitz.calculate(10);
        assertEquals("Blitz", result);
    }

    @Test
    public void testPotzBlitzForMultiplesOfThreeAndFive() {
        String result = potzBlitz.calculate(15);
        assertEquals("PotzBlitz", result);
    }
}