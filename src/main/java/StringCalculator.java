public class StringCalculator {

    int calculate(String input) {
        if (input == null) {
            throw new IllegalArgumentException("input may not be null");
        }
        if (input.isBlank()) {
            return 0;
        }
        if (!input.matches("[0-9 ]+")) {
            throw new IllegalArgumentException("input may only be numbers or space");
        }
        String[] numbers = input.split(" ");
        if (numbers.length > 2) {
            throw new IllegalArgumentException("input may only have two numbers at max");
        }
        int first = Integer.parseInt(numbers[0]);
        if (numbers.length == 1) {
            return first;
        }
        int second = Integer.parseInt(numbers[1]);
        return first + second;
    }
}
