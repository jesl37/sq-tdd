public class PotzBlitz {

    public String calculate(int number) {
        if (number % 3 == 0 && number % 5 == 0) {
            return "PotzBlitz";
        } else if (number % 3 == 0) {
            return "Potz";
        } else if (number % 5 == 0) {
            return "Blitz";
        } else {
            return String.valueOf(number);
        }
    }
}